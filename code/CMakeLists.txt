cmake_minimum_required (VERSION 2.8)
project (berotasuna)
enable_language (Fortran)

#Set the rpath config
set(CMAKE_SKIP_BUILD_RPATH  FALSE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE) 
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
option(BUILD_SHARED_LIBS "Build shared libraries" ON)
set(CMAKE_POSITION_INDEPENDENT_CODE ${BUILD_SHARED_LIBS})

#ajout de la bibliotheque
set(CMAKE_Fortran_MODULE_DIRECTORY ${PROJECT_BINARY_DIR}/mod)
add_subdirectory(lib)

# compile la version sequentielle
add_executable(berotasun_kalkulatu berotasun_sek.f90)
target_link_libraries(berotasun_kalkulatu berotasun)
install(TARGETS berotasun_kalkulatu DESTINATION bin)

# Define the test suite.
enable_testing()
add_test(NAME berotasun_kalkulatu_usage COMMAND $<TARGET_FILE:berotasun_kalkulatu>)
set_tests_properties (berotasun_kalkulatu_usage PROPERTIES PASS_REGULAR_EXPRESSION "Usage*")
add_test(NAME berotasun_kalkulatu_10_err COMMAND $<TARGET_FILE:berotasun_kalkulatu> 10 10 0 0)
set_tests_properties (berotasun_kalkulatu_10_err PROPERTIES PASS_REGULAR_EXPRESSION "1.732")
