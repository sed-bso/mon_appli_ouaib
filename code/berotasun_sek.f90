program berotasun_sek
  use berotasun
  implicit none 
  integer :: nx, ny, size_x, size_y, iter_max, narg, save_flag
  double precision :: hx, hy, dt
  double precision, allocatable ::  u_in(:,:),  u_out(:,:)
  character(len=10)::param
  
  narg = command_argument_count()
  if (narg < 4) then
      call usage()
  end if 
  
  call get_command_argument(1,param)
  read(param,*) nx 
  
  call get_command_argument(2,param)
  read(param,*) ny 

  call get_command_argument(3,param)
  read(param,*) iter_max 
  
  call get_command_argument(4,param)
  read(param,*) save_flag 
  
  hx = 1./ nx;
  hy = 1./ ny
  dt = min(hx*hx/4., hy*hy/4.)

  size_x = nx + 2
  size_y = ny + 2 
  
  allocate(u_in(1:size_x,1:size_y))
  allocate(u_out(1:size_x,1:size_y)) 

  call fix_limites(size_x, size_y, u_in)  
  call fix_limites(size_x, size_y, u_out)  
  
  
  call iter(hx, hy, dt, iter_max, save_flag, 10, size_x, size_y, u_in, u_out)

  deallocate(u_in)
  deallocate(u_out)

contains

subroutine usage()
  implicit none 
  character(len=255)::name
  
  call get_command_argument(0,name)
  print *, 'Usage: ', TRIM(name), ' nx ny iter_max save'
  print *, '    nx       number of discretisation points in X'
  print *, '    ny       number of discretisation points in Y'
  print *, '    iter_max maximal number of iterations in temporal loop'
  print *, '    save     boolean flag (1 or 0) for recording states'
  stop
end subroutine usage


end program berotasun_sek
