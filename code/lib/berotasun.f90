module berotasun
  public :: fix_limites, iter, sauv, bero
  character(len=10)::name_save
contains

  subroutine bero(hx, hy, dt, size_x, size_y, u_in,  u_out, error)
    implicit none
    real(kind=8), intent(in)  :: hx, hy, dt
    integer, intent(in)  :: size_x, size_y
    real(kind=8), intent(in)  :: u_in(1:size_x,1:size_y) 
    real(kind=8), intent(out) :: u_out(1:size_x,1:size_y) 
    real(kind=8), intent(out) :: error
    real(kind=8) :: w_x, w_y, d
    integer :: i, j
    w_x =  dt / (hx * hx)
    w_y =  dt / (hy * hy)
    d = 1.d0 - 2.d0 * w_x - 2.d0 * w_y 

    error = 0.d0
    do i=2, size_x - 1
      do j = 2, size_y - 1
        u_out(i,j) = u_in(i,j) * d + &
          (u_in(i - 1, j) + u_in(i + 1, j)) * w_x + & 
          (u_in(i, j - 1) + u_in(i, j + 1)) * w_y
        error = error + (u_out(i,j) - u_in(i, j))**2
      end do 
    end do   

  end subroutine bero

  subroutine iter(hx, hy, dt, iter_max, save_flag, disp_freq, & 
                  size_x, size_y, u_in, u_out)

    implicit none
    real(kind=8), intent(in)    :: hx, hy, dt
    integer, intent(in)    :: size_x, size_y, iter_max, save_flag, disp_freq
    real(kind=8), intent(inout) :: u_in(size_x, size_y) 
    real(kind=8), intent(inout) :: u_out(size_x, size_y)
    real(kind=8) :: error, prec
    integer :: i

    prec = 1e-4
    error = 1e10
    do i=0, iter_max
      call bero(hx, hy, dt, size_x, size_y, u_in, u_out, error)
      error = sqrt(error)
      if (mod(i,disp_freq) == 0) print * , 'it =', i, 't = ', i*dt, 'err = ', error
      if (save_flag /= 0) then
        write(name_save, '("sol_"i5.5)') i
        call sauv(name_save, size_x, size_y, u_out)
      endif
      u_in = u_out
      if (error <= prec) exit 
    end do 

  end subroutine iter

  subroutine fix_limites(size_x, size_y, u)
    implicit none
    integer, intent(in) :: size_x, size_y 
    real(kind=8), intent(out) :: u(size_x, size_y)

    u(1,       :) = 1.d0
    u(size_x,  :) = 1.d0

    u(:,       1) = 1.d0
    u(:,  size_y) = 1.d0

  end subroutine fix_limites

  subroutine sauv(filename, size_x, size_y, u)
    implicit none
    character(len=*), intent(in) :: filename
    integer, intent(in)   :: size_x, size_y
    real(kind=8), intent(in)   :: u(size_x, size_y)
    integer :: i,j

    open(unit=12,file=trim(filename),form='formatted')
    do i=1, size_x
      do j=1, size_y 
        write(12, '(e15.5)',  advance='no') u(i,j)
      end do
      write(12, '(/)') 
    end do
    close(12)
  end subroutine sauv

end module berotasun
