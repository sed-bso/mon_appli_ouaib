include("bero_funcs.jl")
using WGLMakie
using ColorSchemes
using JSServe, Observables
using JSServe:  Session, App
using JSServe.DOM
using Hyperscript
using Printf

app = App() do session::Session
        
    # simulation parameters
    nx = 50
    ny = nx
    hx = 1. / nx
    hy = 1. / ny
    dt = min(hx*hx/4., hy*hy/4.)
    size_x = nx + 2
    size_y = ny + 2
    iter_max = 100

    # iterative solution matrices
    U_in = zeros(size_x,size_y)
    U_out = zeros(size_x,size_y)

    # init widgets and plots
    potar = Slider(1:100)
    fig = Figure() 
    x = LinRange(0.,1.,size_x) # dummy coordinates
    ax, hm = heatmap(fig[1,1], x, x, Float32.(U_in), colormap = :jet)

    # Observable
    on(potar.value) do val
        α = val * √2 / 100
        set_boundary!(U_in, α)
        for i=1:iter_max # do the simulation
           U_out, error_it = bero_kernel(hx, hy, dt, U_in)
           U_in[2:end-1,2:end-1] = U_out[2:end-1,2:end-1] 
           hm[3][] = Float32.(U_in) # update plot data
        end 
    end     
    potar_nom = DOM.div("α: ", potar, 
                @sprintf("%.3f",potar.value[] * √2 / 100.))
    return JSServe.record_states(session, DOM.div(potar_nom, fig))
end

if isdefined(Main, :server)
    close(server)
end

# starting server
server = JSServe.Server(app, "127.0.0.1", 8082)
JSServe.HTTPServer.start(server)
wait(server)
