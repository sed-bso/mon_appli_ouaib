
include("bero_funcs.jl")

using GLMakie
using ColorSchemes
using Observables


fig = Figure() 
display(fig)
nx = 100
ny = nx
hx = 1. / nx
hy = 1. / ny
dt = min(hx*hx/4., hy*hy/4.)
size_x = nx + 2
size_y = ny + 2
iter_max = 100
x = LinRange(0.,1.,size_x) # des coordonnées bidons
U_in = zeros(size_x,size_y)
U_out = zeros(size_x,size_y)
error_it=1.e10
ax, hm = heatmap(fig[1,1], x, x, U_in .+ 0.5, colormap = :jet)
s = Slider(fig[2,1], range=1:100, startvalue=50)
on(s.selected_index) do val
   α = val * √2 / 100
   set_boundary!(U_in, α)
   for i=1:iter_max
      U_out, error_it = bero_kernel(hx, hy, dt, U_in)
      U_in[2:end-1,2:end-1] = U_out[2:end-1,2:end-1] 
      hm[3] = Float32.(U_in)
      yield()
   end 
end     
