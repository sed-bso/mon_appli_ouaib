# interface with the kernel
function bero_kernel(h_x::Float64, h_y::Float64, dt::Float64, X::Array{Float64,2})
      e = Ref{Float64}(0.)
      size_x = size(X, 1)
      size_y = size(X, 2)
      X_out = similar(X)
      ccall((:__berotasun_MOD_bero, "./libberotasun.so"), 
            Cvoid, (Ref{Float64}, Ref{Float64}, Ref{Float64}, Ref{Int32}, Ref{Int32}, Ptr{Float64},  
      Ptr{Float64}, Ref{Float64}), 
                    h_x         , h_y         , dt          , size_x    , size_y    , X           ,
      X_out       , e)
      return X_out, e[]
end
# fonction d'iterations
function set_boundary!(X::Array{Float64,2}, α::Float64)
    m = size(X,1) 
    n = size(X,2)
    g = (x,y)-> ((x/m)^2+(y/n)^2<=α^2)
    X[1, findall(g.(1,1:n))] .= 1.
    X[findall(g.(1:m,1)), 1] .= 1.
    X[m, findall(g.(m,1:n))] .= 1. 
    X[findall(g.(1:m,n)), n] .= 1.
end

